# symfony-security-http

HTTP Security Integration. https://symfony.com/doc/current/components/security

# Official documentation
* [*Using the new Authenticator-based Security*
  ](https://symfony.com/doc/current/security/authenticator_manager.html)

# Unofficial documentation
* [*Symfony Security Component as a Standalone (Part 1)*
  ](https://medium.com/@amirmodarresi/symfony-security-component-as-a-standalone-part-1-401e842cdd86)
  2018-06 Amir Modarresi
* [*Symfony Security Component as a Standalone (Part 2)*
  ](https://medium.com/@amirmodarresi/symfony-security-component-as-a-standalone-part-2-a2c7a6f5ba58)
  2019-12 Amir Modarresi
* [*Symfony 4.4 Security Cheat Sheet*
  ](https://medium.com/@andreiabohner/symfony-4-4-security-cheat-sheet-5e9e75fbacc0)
  2019-06 Andréia Bohner
  * [*Powerful and complete Security for your apps! Symfony 4.4*
    ](http://assets.andreiabohner.org/symfony/sf44-security-cheat-sheet.pdf)
